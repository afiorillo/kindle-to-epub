import optparse
import os.path

import topaz
import mobidedrm
import process

def decrypt(infile, serial):

    # Which type of book is this?
    ext = ""
    try:
        topaz.cmbtc.bookFile = topaz.cmbtc.openBook(infile)
        topaz.cmbtc.parseTopazHeader()
    except topaz.cmbtc.CMBDTCFatal:
        ext = ".mobi"
    
    outfile = os.path.splitext(infile)[0] + "-decrypted" + ext
    pid = mobidedrm.getPid(serial)
    for error in process.decrypt(infile, outfile, pid):
        print(error)

if __name__ == '__main__':
    parser = optparse.OptionParser("%prog [in_book] [serial]", version="Kindle Book Decrypter")
    options, args = parser.parse_args()
    # print(options, args)
    decrypt(args[0], args[1])